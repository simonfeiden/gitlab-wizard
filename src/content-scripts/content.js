import fillApprovers from './mergeRequests/fillApprovers.js';
import fillOptions from './mergeRequests/fillOptions.js';
import fillLabels from './mergeRequests/fillLabels.js';
import './mergeRequests/resolvedThreads.js';

global.browser = require('webextension-polyfill');

browser.runtime.onMessage.addListener(({ command, payload }) => {
    if (command === 'fillApprovers') {
        fillApprovers(payload);
    } else if (command === 'fillOptions') {
        fillOptions(payload);
    } else if (command === 'fillLabels') {
        fillLabels(payload);
    }
});
