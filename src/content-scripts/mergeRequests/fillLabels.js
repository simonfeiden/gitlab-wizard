import sleep from '../../utils/sleep.js';
import { setValue } from '../../utils/inputUtils.js';

const labelsDropdownMenu = document.querySelector('.dropdown-menu-labels');

const openLabelsDropdown = () => document.querySelector('button[data-field-name="merge_request[label_ids][]"]').click();

const closeLabelsDropdown = () => document.querySelector('.dropdown-menu-labels .dropdown-menu-close').click();

const searchAndSelectLabel = async label => {
    await sleep(100);

    const searchInput = labelsDropdownMenu.querySelector('input[type="search"]');
    searchInput.click();
    setValue(searchInput, label);

    const foundLabels = labelsDropdownMenu.querySelectorAll('.dropdown-content .label-item');

    await sleep(500);

    for (const foundLabel of foundLabels) {
        if (foundLabel.textContent.trim().toLowerCase() === label.toLowerCase()) {
            foundLabel.click();
            break;
        }
    }
};

const fillLabels = async ({ labels }) => {
    if (!labels.length) {
        return;
    }

    openLabelsDropdown();

    await sleep(500);

    for (const label of labels) {
        await searchAndSelectLabel(label);
    }

    closeLabelsDropdown();
};

export default fillLabels;
