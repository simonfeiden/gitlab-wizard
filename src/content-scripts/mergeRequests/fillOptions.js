import { setChecked } from '../../utils/inputUtils.js';

const fillOptions = ({ deleteSourceBranch, squashCommits, assignToMe }) => {
    if (deleteSourceBranch) {
        const checkbox = document.querySelector('#merge_request_force_remove_source_branch');
        setChecked(checkbox);
    }

    if (squashCommits) {
        const checkbox = document.querySelector('#merge_request_squash');
        setChecked(checkbox);
    }

    if (assignToMe) {
        document.querySelector('.merge-request-assignee .assign-to-me-link').click();
    }
};

export default fillOptions;
