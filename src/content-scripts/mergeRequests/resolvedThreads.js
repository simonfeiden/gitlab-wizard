const addBadge = (mergeRequest, resolved, resolvable) => {
    const listItem = document.createElement('li');
    listItem.classList.add('d-none', 'd-sm-flex', 'align-self-center');

    const badge = document.createElement('div');
    badge.classList.add('badge', `badge-pill`, 'color-label');
    badge.textContent = `${resolved}/${resolvable} Threads resolved`;
    badge.style.backgroundColor = resolved === resolvable ? '#87c988' : '#ffb5b7';
    badge.style.color = '#444444';
    listItem.appendChild(badge);

    const controlsList = mergeRequest.querySelector('ul.controls');
    controlsList.classList.add('align-items-center');
    controlsList.prepend(listItem);
};

const addResolvedThreadsBadges = async () => {
    const mergeRequests = document.querySelectorAll('.mr-list > .merge-request');

    for (const mergeRequest of mergeRequests) {
        const url = mergeRequest.querySelector('.merge-request-title a')?.href;

        let resolvedThreads = 0;
        let resolvableThreads = 0;

        fetch(`${url}/discussions.json`)
            .then(response => response.json())
            .then(discussions => {
                discussions.forEach(discussion => {
                    if (discussion.resolved) resolvedThreads++;
                    if (discussion.resolvable) resolvableThreads++;
                });

                if (resolvableThreads) {
                    addBadge(mergeRequest, resolvedThreads, resolvableThreads);
                }
            });
    }
};

if (window.location.pathname.includes('/merge_requests')) {
    addResolvedThreadsBadges();
}
