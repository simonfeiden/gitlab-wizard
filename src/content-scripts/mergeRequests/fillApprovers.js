import sleep from '../../utils/sleep.js';
import { setValue } from '../../utils/inputUtils.js';

const openApprovalGroupModal = () => document.querySelector('button[data-qa-selector="add_approvers_button"]').click();

const submitApprovalGroupModal = () =>
    document.querySelector('#mr-edit-approvals-create-modal___BV_modal_footer_ .btn-success').click();

const searchAndSelectApprover = async approver => {
    const searchInput = document.querySelector(
        'div[data-qa-selector="member_select_field"] .select2-search-field .select2-input'
    );
    searchInput.click();
    setValue(searchInput, approver);

    let suggestions = [];

    const sleepTime = 50;
    let maxWaitTime = 5000;
    while (!suggestions.length) {
        suggestions = document.querySelectorAll('ul.select2-results > li.select2-result-selectable');
        await sleep(sleepTime);
        maxWaitTime -= sleepTime;
        if (maxWaitTime <= 0) return;
    }
    for (const suggestion of suggestions) {
        const suggestionUserName = suggestion.querySelector('.user-username');
        if (suggestionUserName && suggestionUserName.textContent.toLowerCase() === `@${approver.toLowerCase()}`) {
            suggestion.dispatchEvent(
                new MouseEvent('mouseup', {
                    bubbles: true,
                    cancelable: true,
                })
            );
            break;
        }
    }
};

const fillGroup = async ({ name, approvers }) => {
    // Fill group name
    const groupNameInput = document.querySelector('input[data-qa-selector="rule_name_field"]');
    setValue(groupNameInput, name);

    for (const approver of approvers) {
        await searchAndSelectApprover(approver);
    }
};

const fillNumberApprovalsRequired = numberApprovalsRequired => {
    if (numberApprovalsRequired || numberApprovalsRequired === 0) {
        const numberApprovalsRequiredInput = document.querySelector('td.js-approvals-required input');
        setValue(numberApprovalsRequiredInput, numberApprovalsRequired);
    }
};

const fillApprovers = async ({ group, numberApprovalsRequired }) => {
    if (group.approvers.length) {
        openApprovalGroupModal();
        await sleep(500);
        await fillGroup(group);
        submitApprovalGroupModal();
    }

    await sleep(200);

    fillNumberApprovalsRequired(numberApprovalsRequired);
};

export default fillApprovers;
