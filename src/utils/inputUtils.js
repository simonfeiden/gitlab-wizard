/**
 * Sets the value of an input element and emits the input event.
 * @param {Element} inputElement
 * @param value
 */
export const setValue = (inputElement, value) => {
    inputElement.value = value;
    inputElement.dispatchEvent(new Event('input'));
};

/**
 * Sets a checkbox either to checked or not to checked
 * @param {Element} checkboxElement
 * @param {Boolean} checked
 */
export const setChecked = (checkboxElement, checked = true) => {
    checkboxElement.checked = checked;
    checkboxElement.dispatchEvent(new Event('click'));
    checkboxElement.dispatchEvent(new Event('change'));
};
