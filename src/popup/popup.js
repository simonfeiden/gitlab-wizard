import Vue from 'vue';
import { BootstrapVue } from 'bootstrap-vue';
import Popup from './Popup.vue';
import store from '../store';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

global.browser = require('webextension-polyfill');
Vue.prototype.$browser = global.browser;

Vue.use(BootstrapVue);

/* eslint-disable no-new */
new Vue({
    el: '#popup',
    store,
    render: h => h(Popup),
});
