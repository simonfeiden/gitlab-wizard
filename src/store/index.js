import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';

import mergeRequests from './modules/mergeRequests.js';

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
    storage: window.localStorage,
});

/*
 This migrates the old store schema to the new one.
 In the old schema there were no presets.
 TODO Remove after v1.0 is no longer used
 */
const storage = localStorage.getItem('vuex');
if (storage) {
    const parsed = JSON.parse(storage);
    if (parsed?.mergeRequests.approvalGroupName) {
        const oldPreset = parsed.mergeRequests;
        parsed.mergeRequests = {
            presets: [
                {
                    ...oldPreset,
                    presetName: oldPreset.approvalGroupName,
                    approvalGroupName: undefined,
                },
            ],
        };
        localStorage.setItem('vuex', JSON.stringify(parsed));
    }
}

export default new Vuex.Store({
    modules: {
        mergeRequests,
    },
    plugins: [vuexLocal.plugin],
});
